#
# alpine
FROM alpine:3.9

MAINTAINER lgfausak@gmail.com

ENV PG_USER=postgres \
    PG_HOME=/var/lib/postgresql \
    PG_RUNDIR=/run/postgresql \
    PG_LOGDIR=/var/log/postgresql \
    PG_BINDIR=/usr/bin
ENV PG_DATADIR=${PG_HOME}/data

RUN apk update \
 && apk add --no-cache bash bash-doc bash-completion less gawk sed grep bc coreutils bind-tools \
                       util-linux pciutils usbutils coreutils binutils findutils shadow procps openssh py-psycopg2 vim sshpass \
                       python py-pip openssl ca-certificates \
                       python-dev build-base wget curl git py-requests linux-headers sudo libffi-dev openssl-dev python2-dev postgresql \
                       musl-dev make gcc python2 py2-pip \
		       docker jq autoconf automake libtool pv
RUN pip install --upgrade pip requests \
 && pip install jinja2 taskforce dumb-init j2cli hvac python-dateutil netaddr ansible flask flask-cors pyjq jmespath \
 && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
 && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.29-r0/glibc-2.29-r0.apk

RUN apk add glibc-2.29-r0.apk \
 && mkdir -p git usr/local/etc usr/local/bin etc/profile.d /usr/share/ansible/plugins/lookup \
 && adduser -s /bin/bash -h /home/util util -u 1001 -D \
 && echo "set editing-mode vi" > /home/util/.inputrc \
 && chmod u+w /etc/sudoers \
 && echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers \
 && adduser util wheel \
 && sudo -u postgres initdb -D $PG_DATADIR \
 && mkdir -p run/postgresql \
 && chown postgres run/postgresql \
 && sed -i \
      -e "s/.*max_connections.*/max_connections = 300/" \
      -e "\$alisten_addresses = '*'\n" \
      $PG_DATADIR/postgresql.conf \
 && echo 'local all all            trust' > $PG_DATADIR/pg_hba.conf \
 && echo 'host  all all  0.0.0.0/0 trust' >> $PG_DATADIR/pg_hba.conf \
 && echo "export PG_VERSION=${PG_VERSION}" > /etc/profile.d/postgresql.sh \
 && echo "export PG_USER=${PG_USER}" >> /etc/profile.d/postgresql.sh \
 && echo "export PG_HOME=${PG_HOME}" >> /etc/profile.d/postgresql.sh \
 && echo "export PG_RUNDIR=${PG_RUNDIR}" >> /etc/profile.d/postgresql.sh \
 && echo "export PG_BINDIR=${PG_BINDIR}" >> /etc/profile.d/postgresql.sh \
 && echo "export PG_DATADIR=${PG_DATADIR}" >> /etc/profile.d/postgresql.sh \
 && echo "set editing-mode vi" > /root/.inputrc \
 && echo "set mouse=r modeline" > /root/.vimrc \
 && echo "shopt -s histappend" >> ~util/.profile \
 && echo 'export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"' >> ~util/.profile \
 && cp /root/.vimrc /root/.inputrc ~util/. \
 && mkdir -p ~util/git ~util/bin \
 && chown -R util:util ~util \
 && sudo usermod -p util util \
 && rm -f /usr/bin/vi \
 && ln -s /usr/bin/vim  /usr/bin/vi \
 && sudo addgroup util docker

ENV VAULT_ADDR=https://vault.vcp.vzwops.com:8200 \
    PATH="/usr/local/sbin:${PATH}"

COPY Dockerfile /usr/local/etc/.
COPY taskforce.conf /usr/local/etc/.
COPY pgsetup.sh /usr/local/etc/.
COPY startsd /usr/local/bin/startsd
RUN ln /usr/local/bin/startsd /usr/local/bin/start
COPY yamp.py /usr/local/bin/yamp

ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]
CMD [ "/usr/local/bin/start" ]

