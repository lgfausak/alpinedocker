#
# there is a VERSION file with a tuple number, like 1.4.3, the last digit
# is incremented for each build (which docker build inserts into container)
#
# make = just build the avgas image locally
# make publish = tag and push the local avgas:latest to dockerhub.vcp.vzwops.com:5000/avgas:latest
#
F=VERSION
T=/tmp/incver.tmp
C=alpinedocker
V=latest
H=wangotaco
L=local

all: incver
	docker build --no-cache --rm -t $L/$C .
#	docker build --rm -t $L/$C .

publish:
	docker tag $L/$C:$V $H/$C:$V 
	docker push $H/$C:$V

incver:
	awk -F. '{ for (i = 1; i < NF; i++) printf("%s.", $$i); print $$NF+1 }' $F > $T
	mv $T $F

